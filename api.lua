-----------------------------------------------------------------
-- MOD MAGIC API ------------------------------------------------
-----------------------------------------------------------------

dofile(magic.path.."/functions/effects.lua")
dofile(magic.path.."/functions/homewarp.lua")
dofile(magic.path.."/functions/multiblock.lua")
dofile(magic.path.."/functions/altar_rituals.lua")
dofile(magic.path.."/functions/tiers.lua")
dofile(magic.path.."/functions/enchantments.lua")

-----------------------------------------------------------------
-- OTHER UTILITIES ----------------------------------------------
-----------------------------------------------------------------
function magic.is_near_to_magic(pos, radius)
	return minetest.find_node_near(pos, radius, {"magic:magicalcobble", "magic:magicalwater_source", "magic:magicalwater_flowing"})
end