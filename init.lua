-- Definitions made by this mod that other mods can use too
magic = {}

magic.enchants = {}
magic.gfx = {}

magic.playerHomes = {}

magic.recipes = {}
magic.rituals = {}


magic.path = minetest.get_modpath("magic")
magic.worldpath = minetest.get_worldpath()

-- Load files
dofile(magic.path.."/api.lua")

dofile(magic.path.."/items/wand.lua")
dofile(magic.path.."/items/home_stone.lua")

dofile(magic.path.."/nodes/magical_altar.lua")
dofile(magic.path.."/nodes/magical_brewing_stand.lua")
dofile(magic.path.."/nodes/magical_cobble.lua")
dofile(magic.path.."/nodes/magical_hopperbloom.lua")
dofile(magic.path.."/nodes/magical_lightbloom.lua")
dofile(magic.path.."/nodes/magical_obsidian.lua")
dofile(magic.path.."/nodes/magical_water.lua")

dofile(magic.path.."/recipes/t0_begining.lua")
dofile(magic.path.."/recipes/t1_altar.lua")
dofile(magic.path.."/recipes/t1_big_tree.lua")
dofile(magic.path.."/recipes/t1_compass.lua")
dofile(magic.path.."/recipes/t1_extension.lua")
dofile(magic.path.."/recipes/t1_home_stone.lua")
dofile(magic.path.."/recipes/t1_home_warp.lua")
dofile(magic.path.."/recipes/t1_laputa.lua")
dofile(magic.path.."/recipes/t1_perforator.lua")
dofile(magic.path.."/recipes/t1_prospector.lua")
dofile(magic.path.."/recipes/t2_hopperbloom.lua")
dofile(magic.path.."/recipes/t2_lightbloom.lua")


magic.loadHomes()

-- 927 líneas de código