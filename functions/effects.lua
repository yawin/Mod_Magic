-----------------------------------------------------------------
-- GRAPHICAL EFFECTS  -------------------------------------------
-----------------------------------------------------------------
function magic.gfx.altar_ok(pos)
	local spawner = magic.gfx.default()
	spawner.minpos = { x = 0, y = 0, z = 0}
	spawner.maxpos = { x = 0, y = 0, z = 0}
	spawner.minvel = { x = -5, y = 0, z = -5}
	spawner.maxvel = { x = 5, y = 5, z = 5}
	spawner.minacc = { x = 0, y = 0, z = 0}
	spawner.maxacc = { x = 0, y = 0, z = 0}
	spawner.amount = 100
	spawner.time = 0.1
	
	magic.gfx.set_particlespawner(pos,"bubble.png^[colorize:#551A8B:100", spawner)
end

function magic.gfx.recipe_ok(pos)
	local spawner = magic.gfx.default()
	spawner.minpos = { x = -0.5, y = -0.5, z = -0.5}
	spawner.maxpos = { x = 0.5, y = 0.5, z = 0.5}
	spawner.minvel = { x = -3.5, y = -3.5, z = -3.5}
	spawner.maxvel = { x = 3.5, y = 3.5, z = 3.5}
	spawner.minacc = { x = 0, y = 0, z = 0}
	spawner.maxacc = { x = 0, y = 0, z = 0}
	spawner.amount = 50
	spawner.time = 0.1
	
	magic.gfx.set_particlespawner(pos,"bubble.png^[colorize:#551A8B:100", spawner)
end

function magic.gfx.set_particlespawner(pos, _texture, spawner)
	local ps = minetest.add_particlespawner({
		amount = spawner.amount,
		time = spawner.time,
		minpos = {x = pos.x + spawner.minpos.x, y = pos.y + spawner.minpos.y, z = pos.z + spawner.minpos.z},
		maxpos = {x = pos.x + spawner.maxpos.x, y = pos.y + spawner.maxpos.y, z = pos.z + spawner.maxpos.z},
		minvel = spawner.minvel,
		maxvel = spawner.maxvel,
		minacc = spawner.minacc,
		maxacc = spawner.maxacc,
		minexptime = spawner.minexptime,
		maxexptime = spawner.maxexptime,
		minsize = spawner.minsize,
		maxsize = spawner.maxsize,
		collisiondetection = spawner.collisiondetection,
		vertical = spawner.vertical,
		texture = _texture,
	})
	local meta = minetest.get_meta(pos)
	meta:set_string("spawner", ps)
end

function magic.gfx.default()
	return {
			amount = 5, 
			time = 0, 
			minpos = {x = -0.35, y = 0.5, z = -0.35}, 
			maxpos = {x = 0.35, y = 0.5, z = 0.35}, 
			minvel = {x = 0, y = 0, z = 0}, 
			maxvel = {x = 0, y = 0, z = 0}, 
			minacc = {x = 0, y = 0.5, z = 0}, 
			maxacc = {x = 0, y = 1.5, z = 0}, 
			minexptime = 1, 
			maxexptime = 1, 
			minsize = 1, 
			maxsize = 1, 
			collisiondetection = false, 
			vertical = false
	}
end