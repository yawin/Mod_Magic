-----------------------------------------------------------------
-- TIER REGISTERING UTILS ---------------------------------------
-----------------------------------------------------------------
function magic.add_group_to_node(n, group)
   local def = minetest.registered_items[n]
   if not def then
      return false
   end
   
   local dg = def.groups or {}
   for _group, value in pairs(group) do
      if value ~= 0 then
         dg[_group] = value
      else
         dg[_group] = nil
      end
   end
   
   minetest.override_item(n, {groups = dg})
   return true
end

function magic.add_group(group, node_list)
	for i in ipairs(node_list) do
		magic.add_group_to_node(node_list[i], group)
	end
end

-----------------------------------------------------------------
-- TIERS --------------------------------------------------------
-----------------------------------------------------------------
magic.add_group(
	{tier1 = 1},
	{"default:stone", "default:cobble", "default:stonebrick", "default:stone_block", "default:mossycobble", "default:desert_stone", "default:desert_cobble", "default:desert_stonebrick", "default:desert_stone_block", "default:sandstone", "default:sandstonebrick", "default:sandstone_block", "default:dirt", "default:dirt_with_grass", "default:dirt_with_grass_footsteps", "default:dirt_with_dry_grass", "default:sand", "default:desert_sand", "default:silver_sand", "default:gravel", "default:clay"}
)

magic.add_group(
	{tier2 = 1},
	{"default:coalblock","default:diamondblock","default:goldblock","default:obsidian_block"}
)

magic.add_group(
	{t_altar = 1},
	{"default:stone", "default:cobble", "default:mossycobble"}
)