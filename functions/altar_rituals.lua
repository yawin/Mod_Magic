-----------------------------------------------------------------
-- ALTAR RITUALS ------------------------------------------------
-----------------------------------------------------------------
function magic.register_ritual(name, ritual, action)
	magic.rituals[name] = {ritual, action}
end

function magic.do_ritual(node, player)
	local things = magic.get_items(node)
	
	if #things < 1 then
		minetest.chat_send_player(player:get_player_name(), "[Mod magic] The altar is empty")
		return
	end
	
	local itemlist = magic.organice_items(things)
	local n_rit = magic.check_ritual(itemlist)
	
	if n_rit then
		magic.rituals[n_rit][2](node, player)
	end
	
	magic.gfx.altar_ok(node.under)
end

function magic.get_items(node)
	local pos = node.under
	local things = {}
		
	for _,object in ipairs(minetest.env:get_objects_inside_radius(pos, 1)) do
		if not object:is_player() and object:get_luaentity() and object:get_luaentity().name == "__builtin:item" then
			table.insert(things,object:get_luaentity().itemstring)
		end
	end
	
	return things
end

function magic.organice_items(things)
	local did = false
	local itemlist = {}
		
	for i, n in pairs(things) do
		did = false
		
		for j in ipairs(itemlist) do
			if itemlist[j].name == n then
			  itemlist[j].val = itemlist[j].val + 1
			  did = true
			end
		end
		
		if not did then
			itemlist[#itemlist+1] = {name = n, val = 1}
		end
	end
	
	return itemlist
end

function magic.check_ritual(itemlist)
	for ritual_id, ritual_content in pairs(magic.rituals) do
		if #ritual_content[1] == #itemlist then
			local check = false
			for i in ipairs(itemlist) do
				check = magic.check_item_in_ritual(itemlist[i], ritual_content[1])
				
				if not check then
					break
				end
			end
			
			if check then
				return ritual_id
			end
		end
	end
	
	return
end

function magic.check_item_in_ritual(item, itemlist)
	for i in ipairs(itemlist) do
		if itemlist[i].name == item.name and itemlist[i].val == item.val then
			return true
		end
	end
	
	return false
end

function magic.clear_altar(node)
	local pos = node.under
	local things = {}
		
	for _,object in ipairs(minetest.env:get_objects_inside_radius(pos, 1)) do
		if not object:is_player() and object:get_luaentity() and object:get_luaentity().name == "__builtin:item" then
			object:get_luaentity().itemstring = ""
			object:remove()
		end
	end
	
	return things
end