-----------------------------------------------------------------
-- HOME WARP LOGIC ----------------------------------------------
-----------------------------------------------------------------

function magic.loadHomes()
	local fh,err = io.open(magic.worldpath .. "/magic_homes.txt", "r")
	if err then
		print("No existing warps to read.")
		return
	end
	while true do
		local line = fh:read()
		if line == nil then
			break
		end
		local paramlist = string.split(line, " ")
		local w = {
			player = paramlist[1],
			homepos = {
				x = tonumber(paramlist[2]),
				y = tonumber(paramlist[3]),
				z = tonumber(paramlist[4]),
			},
			token = paramlist[5],
		}
		table.insert(magic.playerHomes, w)
	end
	fh:close()
end

function magic.saveHomes()
	local fh,err = io.open(magic.worldpath .. "/magic_homes.txt", "w")
	if err then
		print("No existing warps to read.")
		return
	end
	for i = 1,table.getn(magic.playerHomes) do
		local s = magic.playerHomes[i].player .. " " .. magic.playerHomes[i].homepos.x .. " " .. magic.playerHomes[i].homepos.y .. " " .. magic.playerHomes[i].homepos.z .. " " .. magic.playerHomes[i].token .. "\n"
		fh:write(s)
	end
	fh:close()
end

function magic.checkHome(player)
	for i = 1,table.getn(magic.playerHomes) do
		if magic.playerHomes[i].player == player then
			return i
		end
	end

	return -1
end

function magic.warp(player)
	local playerName = player:get_player_name()
	local home = magic.checkHome(playerName)
	if home == -1 then
		minetest.chat_send_player(playerName, "[Mod Magic] Unknown home")
		return
	end
	
	player:setpos(magic.playerHomes[home].homepos)
	player:set_look_yaw(0)
end