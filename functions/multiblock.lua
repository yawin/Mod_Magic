-----------------------------------------------------------------
-- MULTIBLOCK RECIPES -------------------------------------------
-----------------------------------------------------------------
function magic.register_recipe(name, recipe, action)
	magic.recipes[name] = {recipe, action}
end

function magic.do_spell(itemstack, placer, pointed_thing)
	local name

	if pointed_thing.type == "object" then
		return
	elseif pointed_thing.type == "node" then
		name = minetest.get_node(pointed_thing.under).name
	else
		return
	end

	local npos  = pointed_thing.under
	local npos2 = pointed_thing.above

	local facedir = {x = npos.x - npos2.x, y = npos.y - npos2.y, z = npos.z - npos2.z}

	local coincidences = magic.find_possible_recipes(pointed_thing)

	if #coincidences > 0 then
		for n_rec, rec in ipairs(coincidences) do
			local pt = pointed_thing

			if magic.check_recipe(rec, pt) then
				magic.recipes[rec][2](pt, placer, facedir)
				--return
			end

		end
	end
end
function magic.check_recipe(recipe_id, target)
	local recipe = magic.recipes[recipe_id][1]

	local posx = target.under.x
	local posy = target.under.y
	local posz = target.under.z

	local reference = {x = posx - math.floor(#recipe[1][1] / 2), 
					   y = posy - math.floor(#recipe / 2), 
					   z = posz - math.floor(#recipe[1] / 2)}

	local i = 0
	local j = 0
	local k = 0

	k = 0
	for n_lin, lin in ipairs(recipe) do
		posy = reference.y + k

		j = 0
		for n_col, col in ipairs(lin) do
			posz = reference.z + j

			i = 0
			for n_elem, elem in ipairs(col) do
				posx = reference.x + i

				if elem ~= "" then
					local nodedef = minetest.registered_nodes[minetest.get_node({x = posx,y = posy,z = posz}).name]
					local grupo = false;
					for g, _ in pairs(nodedef.groups) do
						if elem == g then
							grupo = true;
						end
					end
					
					if elem ~= minetest.get_node({x = posx,y = posy,z = posz}).name and not grupo then
						return false
					end
				end

				i = i + 1
			end
			j = j + 1
		end
		k = k + 1
	end

	return true
end

function magic.find_possible_recipes(node)
	local to_ret = {}
	local coincide
	local nodedef = minetest.registered_nodes[minetest.get_node(node.under).name]

	for n_rec, rec in pairs(magic.recipes) do
		coincide = false

		for n_lin, lin in ipairs(rec[1]) do
			for n_col, col in ipairs(lin) do
				for n_elem, elem in ipairs(col) do
					local grupo = false;
					for g, _ in pairs(nodedef.groups) do
						if elem == g then
							grupo = true;
						end
					end
					if elem == minetest.get_node(node.under).name or grupo then
						to_ret[#to_ret+1] = n_rec

						coincide = true
					end

					if coincide then break end
				end

				if coincide then break end
			end

			if coincide then break end
		end
	end

	return to_ret
end