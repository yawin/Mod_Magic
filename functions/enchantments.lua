-----------------------------------------------------------------
-- ENCHANTMENTS LOGIC -------------------------------------------
-----------------------------------------------------------------
function minetest.handle_node_drops(pos, drops, digger)
	magic.use_tool(pos, drops, digger)
end

function magic.use_tool(pos, drops, digger)
	if not digger or digger:get_wielded_item():to_string() == "" then return end

	local item = digger:get_wielded_item()
	local meta = item:get_meta()
	
	local actions = {}
	local nodeinfo = {drop={}}
	for i in ipairs(drops) do
		nodeinfo.drop[i] = drops[i]
		--minetest.chat_send_all(drops[d])
	end
	
	for e_id, enchantment in pairs(magic.enchants) do
		if enchantment.type == "on_dig" then
			local value = meta:get_string(e_id)
			if value ~= "" then
				actions[#actions + 1] = {val = tonumber(value), action = enchantment.action}
			end
		end
	end
	
	if #actions == 0 then
		for d in pairs(nodeinfo.drop) do
			minetest.add_item(pos, nodeinfo.drop[d])
		end
		return
	end
	
	for i in ipairs(actions) do
		nodeinfo.drop = actions[i].action(actions[i].val, pos, digger, nodeinfo)
	end
	
	for d in pairs(nodeinfo.drop) do
		minetest.add_item(pos, nodeinfo.drop[d])
	end
end

function magic.register_on_dig_enchant(enchant_id, enchant_type, value_max, action_function, ingredient, enchantables)
	magic.enchants[enchant_id] = 
	{
		type = enchant_type,
		max = value_max,
		action = action_function
	}
	
	if #enchantables == 0 then
		return
	end
	
	for i in ipairs(enchantables) do
 		magic.register_ritual(enchant_id .. "_" .. enchantables[i],
 			{
 				{name = enchantables[i], val = 1},
 				{name = ingredient, val = 1},
 			},
 			
 			function(node, player)
 				for _,object in ipairs(minetest.env:get_objects_inside_radius(node.under, 1)) do
 					if not object:is_player() and object:get_luaentity() and
					   object:get_luaentity().name == "__builtin:item" then
						if object:get_luaentity().itemstring == enchantables[i] then
							local item = ItemStack(object:get_luaentity().itemstring)
							local meta = item:get_meta()
							local def = minetest.registered_items[item:get_name()]
							meta:set_string("description", def.description .. " (" .. enchant_id .. ")")
							meta:set_string("inventory_image", def.inventory_image.."^[colorize:#551A8B:120")
							meta:set_string(enchant_id, "3")

							object:remove()
							minetest.env:add_item(node.under, item)
						else
							object:remove()
						end
 					end
 				end
 			end
 		)
	end
end

-----------------------------------------------------------------
-- ENCHANTMENTS -------------------------------------------------
-----------------------------------------------------------------
magic.register_on_dig_enchant("luck", "on_dig", 3, 
	function(luck_value, pos, digger, nodeinfo)
		local drop_count = math.random(2, luck_value + 1) + 1
		
		for i in ipairs(nodeinfo.drop) do
			nodeinfo.drop[i] = nodeinfo.drop[i].." "..drop_count
		end

		return nodeinfo.drop
	end,
	
	"default:cobble",
	
	{
		"default:pick_diamond",
		"default:shovel_diamond",
		"default:axe_diamond",
		
		"default:pick_steel",
		"default:shovel_steel",
		"default:axe_steel",
		
		"default:pick_stone",
		"default:shovel_stone",
		"default:axe_stone",
		
		"default:pick_wood",
		"default:shovel_wood",
		"default:axe_wood"
	}
)
magic.register_on_dig_enchant("silk_touch", "on_dig", 3, 
	function(luck_value, pos, digger, nodeinfo)
		nodeinfo.drop = {minetest.get_node(pos).name};
		return nodeinfo.drop
	end,
	
	"default:wood",
	
	{
		"default:pick_diamond",
		"default:shovel_diamond",
		"default:axe_diamond",
		
		"default:pick_steel",
		"default:shovel_steel",
		"default:axe_steel",
		
		"default:pick_stone",
		"default:shovel_stone",
		"default:axe_stone",
		
		"default:pick_wood",
		"default:shovel_wood",
		"default:axe_wood"
	}
)
magic.register_on_dig_enchant("cook", "on_dig", 3, 
	function(luck_value, pos, digger, nodeinfo)
		local tdrop
		local ltdrop = {}

		for i in ipairs(nodeinfo.drop) do
			tdrop = minetest.get_craft_result({ method = "cooking", width = 1, items = {nodeinfo.drop[i]}})

			ltdrop[#ltdrop + 1] = tdrop.item:to_table().name
		end

		return ltdrop
	end,
	
	"default:coal_lump",
	
	{
		"default:pick_diamond",
		"default:shovel_diamond",
		"default:axe_diamond",
		
		"default:pick_steel",
		"default:shovel_steel",
		"default:axe_steel",
		
		"default:pick_stone",
		"default:shovel_stone",
		"default:axe_stone",
		
		"default:pick_wood",
		"default:shovel_wood",
		"default:axe_wood"
	}
)