minetest.register_node("magic:magical_altar", {
	description = "Magical altar",
	drawtype = "nodebox",
	tiles = {"altar_top.png","altar_top.png","altar_side.png"},
	paramtype = "light",
	paramtype2 = "facedir",
	is_ground_content = false,
	node_box = {
		type = "fixed",
		fixed = {-0.5, -0.5, -0.5, 0.5, -0.19, 0.5},
	},
	selection_box = node_box,
	collision_box = node_box,
	groups = {cracky = 2, oddly_breakable_by_hand=1},
	sounds = default.node_sound_stone_defaults(),

	on_rightclick = function(pos, node, player, itemstack, pointed_thing)
		local p = pos
		p.x = p.x + math.random(-4, 4)/10
		p.z = p.z + math.random(-4, 4)/10
		
		minetest.env:add_item(p, itemstack:get_name())
		itemstack:take_item()
		player:set_wielded_item(itemstack)
	end
})

minetest.override_item("stairs:slab_stone_block",{
	liquids_pointable = true,
})