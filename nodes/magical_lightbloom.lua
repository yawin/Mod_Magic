minetest.register_node("magic:lightbloom", {
	description = "Lightbloom",
	drawtype = "plantlike",
	waving = 1,
	tiles = {"magic_lightbloom.png"},
	inventory_image = "magic_lightbloom.png",
	wield_image = "magic_lightbloom.png",
	light_source = 15,
	
	sunlight_propagates = true,
	paramtype = "light",
	walkable = false,
	stack_max = 64,
	groups = {snappy = 3, flower = 1, flora = 1, attached_node = 1},
	sounds = default.node_sound_leaves_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-2 / 16, -0.5, -2 / 16, 2 / 16, 2 / 16, 2 / 16},
	}
})