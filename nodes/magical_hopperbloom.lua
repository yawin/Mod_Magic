minetest.register_node("magic:hopperbloom", {
	description = "Hopperbloom",
	drawtype = "plantlike",
	waving = 1,
	tiles = {"magic_hopperbloom.png"},
	inventory_image = "magic_hopperbloom.png",
	wield_image = "magic_hopperbloom.png",
	
	sunlight_propagates = true,
	paramtype = "light",
	walkable = false,
	stack_max = 64,
	groups = {snappy = 3, flower = 1, flora = 1, attached_node = 1},
	sounds = default.node_sound_leaves_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-2 / 16, -0.5, -2 / 16, 2 / 16, 2 / 16, 2 / 16},
	}
})


minetest.register_abm({
	nodenames = {"magic:hopperbloom"},
	neighbors = {"default:chest"},
	interval = 1.0,
	chance = 1,
	
	action = function(pos, node, active_object_count, active_object_count_wider)
		local meta
		local inv
		
		local min = {x=pos.x-1,y=pos.y-1,z=pos.z-1}
		local max = {x=pos.x+1,y=pos.y+1,z=pos.z+1}
		local vm = minetest.get_voxel_manip()	
		local emin, emax = vm:read_from_map(min,max)
		local area = VoxelArea:new{MinEdge=emin, MaxEdge=emax}
		local data = vm:get_data()

		local storages = {}
		
		for i = 0, 2 do
			for j = 0, 2 do
				for k = 0, 2 do
					if vm:get_node_at({x=min.x + i, y=min.y + j, z=min.z + k}).name == "default:chest" then
						storages[#storages + 1] = {x=min.x + i, y=min.y + j, z=min.z + k}
					end
				end
			end
		end 
		
		for _,object in ipairs(minetest.get_objects_inside_radius(pos, 5)) do
			if not object:is_player() and object:get_luaentity() and object:get_luaentity().name == "__builtin:item" then
				for it in pairs(storages) do
					meta = minetest.get_meta(storages[it]);
					inv = meta:get_inventory()
					
					if inv and inv:room_for_item("main", ItemStack(object:get_luaentity().itemstring)) then
						inv:add_item("main", ItemStack(object:get_luaentity().itemstring))
						object:get_luaentity().itemstring = ""
						object:remove()
					end
				end
			end
		end
	end,
})