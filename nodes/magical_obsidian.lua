minetest.register_node("magic:magicalobsidian", {
	description = "Magical Obsidian",
	tiles = {
		{
			name = "magic_magicalobsidian_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 10.0,
			},
		},
	},
	special_tiles = {
		-- New-style water source material (mostly unused)
		{
			name = "magic_magicalobsidian_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 10.0,
			},
			--backface_culling = false,
		},
	},
	
	after_dig_node = function(pos, oldnode, oldmetadata)
		local meta = minetest.env:get_meta(pos)
		meta:from_table(oldmetadata)
		
		local s = meta:get_string("magic_referido")
		
		local home = magic.checkHome(s)
		if home ~= -1 then
			if magic.playerHomes[home].token == meta:get_string("magic_token") then
				table.remove(magic.playerHomes,home)
				magic.saveHomes()
			end
		end
		return
	end,

	drop = "default:obsidian",
	light_source = 5,
	paramtype = "light",
	groups = {cracky = 2, stone = 1},
	sounds = default.node_sound_stone_defaults(),
})