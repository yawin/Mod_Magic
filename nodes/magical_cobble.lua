-- Agua mágica, su cubo y la fuenta donde se puede encontrar

minetest.register_node("magic:magicalcobble", {
	description = "Magical Cobblestone",
	tiles = {
		{
			name = "magic_magicalcobble_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 10.0,
			},
		},
	},
	special_tiles = {
		-- New-style water source material (mostly unused)
		{
			name = "magic_magicalcobble_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 10.0,
			},
			--backface_culling = false,
		},
	},
	light_source = 5,
	paramtype = "light",
	drop = "default:cobble",
	groups = {cracky = 2, stone = 1},
	sounds = default.node_sound_stone_defaults(),
})

walls.register("magic:magicalcobble_wall", "Magical Cobblestone Wall", "magic_magicalcobble.png",
		"magic:magicalcobble", default.node_sound_stone_defaults())
minetest.override_item("magic:magicalcobble_wall",{
	tiles = {
		{
			name = "magic_magicalcobble_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 10.0,
			},
		},
	},
	special_tiles = {
		-- New-style water source material (mostly unused)
		{
			name = "magic_magicalcobble_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 10.0,
			},
			--backface_culling = false,
		},
	},
	light_source = 5,
	paramtype = "light",
	drop = "walls:cobble",
})
