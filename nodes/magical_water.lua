-----------------------------------------------------------------
-- MAGICAL WATER AND THE BUCKET ---------------------------------
-----------------------------------------------------------------

local function get_wand(pos, node, player, itemstack, pointed_thing)
	local p = pos
	p.y = p.y + 1
	
	if itemstack:get_name() == "default:stick" then
		minetest.env:add_item(p, "magic:wand")
		
		itemstack:take_item()
		player:set_wielded_item(itemstack)
	elseif itemstack:get_name() == "stairs:slab_stone_block" then
		minetest.env:add_item(p, "magic:magical_altar")
		
		itemstack:take_item()
		player:set_wielded_item(itemstack)
	end
end
minetest.register_node("magic:magicalwater_source", {
	description = "Magical Water Source",
	drawtype = "liquid",
	tiles = {
		{
			name = "magic_magicalwater_source_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
		},
	},
	special_tiles = {
		-- New-style water source material (mostly unused)
		{
			name = "magic_magicalwater_source_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
			backface_culling = false,
		},
	},
	light_source = 5,
	alpha = 160,
	paramtype = "light",
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	is_ground_content = false,
	drop = "",
	drowning = 1,
	liquidtype = "source",
	liquid_alternative_flowing = "magic:magicalwater_flowing",
	liquid_alternative_source = "magic:magicalwater_source",
	liquid_viscosity = 1,
	post_effect_color = {a = 103, r = 30, g = 60, b = 90},
	groups = {water = 3, liquid = 3, puts_out_fire = 1, cools_lava = 1},
	sounds = default.node_sound_water_defaults(),
	
	on_rightclick = function(pos, node, player, itemstack, pointed_thing)
		get_wand(pos, node, player, itemstack, pointed_thing)
	end,
})

minetest.register_node("magic:magicalwater_flowing", {
	description = "Flowing Magical Water",
	drawtype = "flowingliquid",
	tiles = {"magic_magicalwater.png"},
	special_tiles = {
		{
			name = "magic_magicalwater_flowing_animated.png",
			backface_culling = false,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 0.8,
			},
		},
		{
			name = "magic_magicalwater_flowing_animated.png",
			backface_culling = true,
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 0.8,
			},
		},
	},
	light_source = 5,
	alpha = 160,
	paramtype = "light",
	paramtype2 = "flowingliquid",
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	is_ground_content = false,
	drop = "",
	drowning = 1,
	liquidtype = "flowing",
	liquid_alternative_flowing = "magic:magicalwater_flowing",
	liquid_alternative_source = "magic:magicalwater_source",
	liquid_viscosity = 1,
	post_effect_color = {a = 103, r = 30, g = 60, b = 90},
	groups = {water = 3, liquid = 3, puts_out_fire = 1,
		not_in_creative_inventory = 1, cools_lava = 1},
	sounds = default.node_sound_water_defaults(),
	
	on_rightclick = function(pos, node, player, itemstack, pointed_thing)
		get_wand(pos, node, player, itemstack, pointed_thing)
	end,
})

bucket.register_liquid(
	"magic:magicalwater_source",
	"magic:magicalwater_flowing",
	"magic:bucket_magicalwater",
	"bucket_magicalwater.png",
	"Magical Water Bucket",
	{water_bucket = 1}
)
