minetest.register_node("magic:brewing_stand", {
	description = "Brewing Stand",
	groups = {cracky=1, oddly_breakable_by_hand=1},

	sounds = default.node_sound_stone_defaults(),
	
	tiles = {"brewing_stand_base.png","altar_top.png","altar_side.png"},
	paramtype = "light",
	drawtype = "nodebox",
	
	node_box = {
		type = "fixed",
		fixed = {-0.5, -0.5, -0.5, 0.5, -0.19, 0.5},
	},
	
	on_rightclick = function(pos, node, clicker, item, _)
		
	end,
})
	
	
--[[tiles = {
		"brewing_stand_base.png",
		"brewing_stand_base.png",
		"brewing_stand.png",
		"brewing_stand.png",
		"brewing_stand.png",
		"brewing_stand.png"
	node_box = {
		type = "fixed",
		fixed = {
			{-0.25, -0.5, -0.25, 0.25, -0.4375, 0.25}, -- NodeBox1
			{-0.0625, -0.5, -0.0625, 0.0625, 0, 0.0625}, -- NodeBox2
			{-0.5, 0, -0.125, 0.5, 0.0625, 0.125}, -- NodeBox3
			{-0.4375, -0.1875, -0.0625, -0.3125, 0.375, 0.0625}, -- NodeBox4
			{0.3125, -0.1875, -0.0625, 0.4375, 0.375, 0.0625}, -- NodeBox5
			{-0.125, 0.0625, -0.125, 0.125, 0.125, 0.125}, -- NodeBox6
			{-0.1875, 0.125, -0.125, 0.1875, 0.375, 0.125}, -- NodeBox7
			{-0.125, 0.125, -0.1875, 0.125, 0.375, 0.1875}, -- NodeBox8
			{-0.0625, 0.375, -0.0625, 0.0625, 0.5, 0.0625}, -- NodeBox9
		}
	},]]--