 minetest.register_craftitem("magic:home_stone",{
	description = "Home stone",
	inventory_image = "home_stone.png",
	stack_max = 1,

	on_use = function(itemstack, placer, pointed_thing)
		magic.warp(placer)
	end,
})