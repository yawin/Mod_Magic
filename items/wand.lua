-----------------------------------------------------------------
-- WAND  -------------------------------------------
-----------------------------------------------------------------
minetest.register_craftitem("magic:wand",{
	description = "Magical wand",
	inventory_image = "magic_wand.png",
	stack_max = 1,

	on_place = function(itemstack, placer, pointed_thing)
		magic.do_spell(itemstack, placer, pointed_thing)
	end,
})

minetest.override_item("default:stick",{
	liquids_pointable = true,
})

minetest.override_item("default:leaves",{
	on_rightclick = function(pos, node, player, itemstack, pointed_thing)
		if itemstack:get_name() == "default:sapling" then
			if magic.check_recipe("begining", pointed_thing) then
				magic.recipes["begining"][2](pointed_thing)
			end
		end
	end,
})
	--[[on_use = function(itemstack, user, pointed_thing)
		if pointed_thing.type == "object" then
			pointed_thing.ref:punch(user, 1.0, { full_punch_interval=1.0 }, nil)
			return user:get_wielded_item()
		elseif pointed_thing.type ~= "node" then
			-- do nothing if it's neither object nor node
			return
		end

		local node = minetest.get_node(pointed_thing.under)
		local liquiddef = bucket.liquids[node.name]
		local item_count = user:get_wielded_item():get_count()

		if liquiddef ~= nil and liquiddef.itemname ~= nil and (node.name == "magic:magicalwater_source" or node.name == "magic:magicalwater_flowing") then
			return ItemStack("magic:wand")
		elseif node.name == "default:leaves" then
			if magic.check_recipe("begining", pointed_thing) then
				magic.recipes["begining"][2](pointed_thing)
			end
		end

		--return itemstack
	end,



			minetest.env:add_item(user:get_pos(), "magic:wand")
			itemstack:take_item()
			return itemstack
]]--