-----------------------------------------------------------------
-- LAPUTA RECIPE ------------------------------------------------
-----------------------------------------------------------------

function magic.move_node(pos, newpos)
	local node = minetest.get_node(pos)
	if node.name == "ignore" then
		return
	end

	local newnode = minetest.get_node(newpos)
	local def = minetest.registered_nodes[newnode.name]
	if def and def.buildable_to then
		local meta = minetest.get_meta(pos):to_table()
		minetest.set_node(newpos, node)
		minetest.get_meta(newpos):from_table(meta)
		minetest.remove_node(pos)
	end
end

magic.register_recipe("laputa",
	{
		{
			{"default:goldblock", "tier2", "default:goldblock",},
			{"tier2", "default:goldblock", "tier2",},
			{"default:goldblock", "tier2", "default:goldblock",},
		},
	},
	function(node)
		local posx = node.under.x
		local posy = node.under.y
		local posz = node.under.z
		
		local n = minetest.get_node({x = posx - 1, y = posy, z = posz}).name

		if minetest.get_node({x = posx + 1, y = posy, z = posz}).name ~= n or
		   minetest.get_node({x = posx, y = posy, z = posz - 1}).name ~= n or
		   minetest.get_node({x = posx, y = posy, z = posz + 1}).name ~= n then
			 return
		end
		
		local range = 5
		if n == "default:coalblock" then
			 range = range * 2
		elseif n == "default:goldblock" then
			 range = range * 3
		elseif n == "default:diamondblock" then
			 range = range * 4
		end
		
		local incy = posy + (range * 10)
		local incx = -range
		local incz = -range
		posy = posy - range
		
		-- Fill selected area with node
		while posy <= incy do
			while incx <= range do
				while incz <= range do
					if minetest.get_node({x = posx + incx, y = incy, z = posz + incz}).name ~= "air" then
						--minetest.set_node({x = posx + incx, y = incy, z = posz + incz}, {name = "default:glass"})
						magic.move_node({x = posx + incx, y = incy, z = posz + incz},{x = posx + incx, y = incy + range*10, z = posz + incz})
					end
					incz = incz + 1
				end
				incz = -range
				incx = incx + 1
			end
			incx = -range
			incy = incy - 1
		end

		magic.gfx.recipe_ok(node.under)
	end
)



--[[minetest.register_node("magic:laputa_pointer", {
	drawtype = "airlike",
	walkable = false,
	pointable = false,
	diggable = false,
	climbable = false,
	buildable_to = true,
	sunlight_propagates = true,
	paramtype = "light",
	
	on_construct = function(pos)
		minetest.get_node_timer(pos):set(10, 0)
		minetest.get_node_timer(pos):start(10)
	end,
	on_timer = function(pos, elapsed)
		minetest_chat_send_all("Pito")
		return 10
	end,
})
]]--