-----------------------------------------------------------------
-- BIG TREE RECIPE ----------------------------------------------
-----------------------------------------------------------------

-- ToDo: Spawn different trees for different randomly and 
--		 different type of tree for different type of sappling

magic.register_recipe("big_tree",
	{
		{
			{"default:dirt_with_grass", "field", "default:dirt_with_grass",},
			{"field", "", "field",},
			{"field", "default:dirt_with_grass", "field",},
		},
		{
			{"air", "air", "air",},
			{"air", "default:sapling", "air",},
			{"air", "air", "air",},
		},
	},
	function(node)
		local posx = node.under.x - 5
		local posy = node.under.y
		local posz = node.under.z - 7

		local sch = magic.path .. "/schems/magical_big_tree.mts"
		minetest.place_schematic({x = posx, y = posy, z = posz}, sch, 0, nil, true)

		magic.gfx.recipe_ok(node.under)
	end
)