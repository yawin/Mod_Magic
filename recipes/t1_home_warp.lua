-----------------------------------------------------------------
-- HOME WARP RECIPE ---------------------------------------------
-----------------------------------------------------------------

magic.register_recipe("home_warp",
	{
		{
			{"","default:obsidian","default:obsidian","default:obsidian",""},
			{"default:obsidian","","","","default:obsidian"},
			{"","","default:obsidian","","default:obsidian"},
			{"","default:obsidian","","","default:obsidian"},
			{"","","default:obsidian","default:obsidian",""},
		},
	},
	
	function(node, player)
		local posx = node.under.x
		local posy = node.under.y
		local posz = node.under.z
		
		local i = -2
		local j = -2
		
		
		local arrID = magic.checkHome(player:get_player_name())
		
		local token = node.under.x .. node.under.y .. node.under.z
		
		if arrID == -1 then
			arrID = #magic.playerHomes + 1
		end
		
		magic.playerHomes[arrID] = {}
		magic.playerHomes[arrID].player = player:get_player_name()
		magic.playerHomes[arrID].homepos = {x = node.under.x, y = node.under.y + 1, z = node.under.z}
		magic.playerHomes[arrID].token = token
		
		magic.saveHomes()
		
		while i <= 2 do
			while j <= 2 do
				if "default:obsidian" == minetest.get_node({x = posx + i, y = posy, z = posz + j}).name then
					minetest.set_node({x = posx + i, y = posy, z = posz + j}, {name="magic:magicalobsidian"})
					
					local meta = minetest.get_meta({x = posx + i, y = posy, z = posz + j})
					meta:set_string("magic_referido",player:get_player_name())
					meta:set_string("magic_token",token)
				end
				j = j + 1
			end
			j = -2
			
			i = i + 1
		end

		magic.gfx.recipe_ok(node.under)
	end
)