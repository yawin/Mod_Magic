-----------------------------------------------------------------
-- EXTENSION RECIPE ---------------------------------------------
-----------------------------------------------------------------
local function extension_action(node, player, facedir)
	local posx = node.under.x
	local posy = node.under.y
	local posz = node.under.z

	local base = {}
	base[0] = minetest.get_node({x = posx - facedir.z, y = posy - 1, z = posz - facedir.x}).name
	base[1] = minetest.get_node({x = posx , y = posy - 1, z = posz}).name
	base[2] = minetest.get_node({x = posx + facedir.z, y = posy - 1, z = posz + facedir.x}).name

	local n = ""
	n = minetest.get_node({x = posx, y = posy + 1, z = posz}).name

	local range = 5
	if n == "default:coalblock" then
		 range = range * 2
	elseif n == "default:goldblock" then
		 range = range * 4
	elseif n == "default:diamondblock" then
		 range = range * 8
	else
		return
	end

	local item = ""
	local nodo
	for i = 1, range do
		nodo = minetest.get_node({x = posx + facedir.z + facedir.x*i, y = posy - 1, z = posz - facedir.x + facedir.z*i})
		item = nodo.name
		
		if item == "air" then
			minetest.set_node({x = posx + facedir.z + facedir.x*i, y = posy - 1, z = posz - facedir.x + facedir.z*i},{name = base[0]})
		end
		
		item = minetest.get_node({x = posx + facedir.x*i, y = posy - 1, z = posz + facedir.z*i}).name
		if item == "air" then
			minetest.set_node({x = posx + facedir.x*i, y = posy - 1, z = posz + facedir.z*i},{name = base[1]})
		end
			
		item = minetest.get_node({x = posx - facedir.z + facedir.x*i, y = posy - 1, z = posz + facedir.x + facedir.z*i}).name
		if item == "air" then
			minetest.set_node({x = posx - facedir.z + facedir.x*i, y = posy - 1, z = posz + facedir.x + facedir.z*i},{name = base[2]})
		end
	end
	minetest.set_node({x = posx, y = posy + 1, z = posz}, {name="air"})
end

magic.register_recipe("extension",
	{
		{
			{"", "",""},
			{"default:obsidian", "default:obsidian","default:obsidian"},
			{"", "", ""},
		},
	},
	function(node, player, facedir)
		extension_action(node, player, facedir)

		magic.gfx.recipe_ok(node.under)
	end
)

magic.register_recipe("extension2",
	{
		{
			{"", "default:obsidian",""},
			{"", "default:obsidian",""},
			{"", "default:obsidian", ""},
		},
	},
	function(node, player, facedir)
		extension_action(node, player, facedir)

		magic.gfx.recipe_ok(node.under)
	end
)