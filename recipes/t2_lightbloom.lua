magic.register_ritual("lightbloom",
	{
		{name = "flowers:geranium", val = 1}
	},
	
	function(node, player)
		magic.clear_altar(node)
		minetest.env:add_item(node.under, "magic:lightbloom")
	end
)