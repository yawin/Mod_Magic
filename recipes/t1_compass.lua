-----------------------------------------------------------------
-- COMPASS RECIPE -----------------------------------------------
-----------------------------------------------------------------

magic.register_recipe("compass",
	{
		{
			{"tier1", "air", "tier1",},
			{"air", "tier1", "air",},
			{"tier1", "air", "tier1",},
		},
	},
	function(node)
		local posx = node.under.x
		local posy = node.under.y
		local posz = node.under.z

		local nod = minetest.get_node({x = posx - 1, y = posy, z = posz + 1})
		minetest.set_node({x = posx - 1, y = posy, z = posz}, {name = nod.name})
		minetest.set_node({x = posx - 1, y = posy, z = posz + 1}, {name = "air"})
		
		nod = minetest.get_node({x = posx + 1, y = posy, z = posz + 1})
		minetest.set_node({x = posx + 1, y = posy, z = posz}, {name = nod.name})
		minetest.set_node({x = posx + 1, y = posy, z = posz + 1}, {name = "air"})
		
		nod = minetest.get_node(node.under)
		minetest.set_node({x = posx, y = posy, z = posz + 1}, {name = nod.name})
		minetest.set_node({x = posx, y = posy, z = posz}, {name = "air"})

		magic.gfx.recipe_ok(node.under)
	end
)