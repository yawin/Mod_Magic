magic.register_recipe("altar",
	{
		{
			{"", "", "", "", "", "tier1", "tier1", "tier1", "", "", "", "", ""},
			{"", "", "", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "", "", ""},
			{"", "", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "", ""},
			{"", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", ""},
			{"", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", ""},
			{"tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1"},
			{"tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1"},
			{"tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1"},
			{"", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", ""},
			{"", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", ""},
			{"", "", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "", ""},
			{"", "", "", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "tier1", "", "", ""},
			{"", "", "", "", "", "tier1", "tier1", "tier1", "", "", "", "", ""},
		},
		{
			{"", "", "", "", "", "default:stone_block", "air", "default:stone_block", "", "", "", "", ""},
			{"", "", "", "air", "default:stone_block", "air", "air", "air", "default:stone_block", "air", "", "", ""},
			{"", "", "default:stone_block", "air", "air", "air", "air", "air", "air", "air", "default:stone_block", "", ""},
			{"", "air", "air", "air", "air", "air", "air", "air", "air", "air", "air", "air", ""},
			{"", "default:stone_block", "air", "air", "air", "air", "air", "air", "air", "air", "air", "default:stone_block", ""},
			{"default:stone_block", "air", "air", "air", "air", "stairs:slab_stone_block", "stairs:stair_stone_block", "stairs:slab_stone_block", "air", "air", "air", "air", "default:stone_block"},
			{"air", "air", "air", "air", "air", "stairs:stair_stone_block", "default:stone_block", "stairs:stair_stone_block", "air", "air", "air", "air", "air"},
			{"default:stone_block", "air", "air", "air", "air", "stairs:slab_stone_block", "stairs:stair_stone_block", "stairs:slab_stone_block", "air", "air", "air", "air", "default:stone_block"},
			{"", "default:stone_block", "air", "air", "air", "air", "air", "air", "air", "air", "air", "default:stone_block", ""},
			{"", "air", "air", "air", "air", "air", "air", "air", "air", "air", "air", "air", ""},
			{"", "", "default:stone_block", "air", "air", "air", "air", "air", "air", "air", "default:stone_block", "", ""},
			{"", "", "", "air", "default:stone_block", "air", "air", "air", "default:stone_block", "air", "", "", ""},
			{"", "", "", "", "", "default:stone_block", "air", "default:stone_block", "", "", "", "", ""},
		},
		{
			{"", "", "", "", "", "t_altar", "air", "t_altar", "", "", "", "", ""},
			{"", "", "", "air", "t_altar", "air", "air", "air", "t_altar", "air", "", "", ""},
			{"", "", "t_altar", "air", "air", "air", "air", "air", "air", "air", "t_altar", "", ""},
			{"", "air", "air", "air", "air", "air", "air", "air", "air", "air", "air", "air", ""},
			{"", "t_altar", "air", "air", "air", "air", "air", "air", "air", "air", "air", "t_altar", ""},
			{"t_altar", "air", "air", "air", "air", "air", "air", "air", "air", "air", "air", "air", "t_altar"},
			{"air", "air", "air", "air", "air", "air", "magic:magical_altar", "air", "air", "air", "air", "air", "air"},
			{"t_altar", "air", "air", "air", "air", "air", "air", "air", "air", "air", "air", "air", "t_altar"},
			{"", "t_altar", "air", "air", "air", "air", "air", "air", "air", "air", "air", "t_altar", ""},
			{"", "air", "air", "air", "air", "air", "air", "air", "air", "air", "air", "air", ""},
			{"", "", "t_altar", "air", "air", "air", "air", "air", "air", "air", "t_altar", "", ""},
			{"", "", "", "air", "t_altar", "air", "air", "air", "t_altar", "air", "", "", ""},
			{"", "", "", "", "", "t_altar", "air", "t_altar", "", "", "", "", ""},
		},
		{
			{"", "", "", "", "", "t_altar", "t_altar", "t_altar", "", "", "", "", ""},
			{"", "", "", "t_altar", "t_altar", "air", "air", "air", "t_altar", "t_altar", "", "", ""},
			{"", "", "t_altar", "air", "air", "air", "air", "air", "air", "air", "t_altar", "", ""},
			{"", "t_altar", "air", "air", "air", "air", "air", "air", "air", "air", "air", "t_altar", ""},
			{"", "t_altar", "air", "air", "air", "air", "air", "air", "air", "air", "air", "t_altar", ""},
			{"t_altar", "air", "air", "air", "air", "air", "air", "air", "air", "air", "air", "air", "t_altar"},
			{"t_altar", "air", "air", "air", "air", "air", "air", "air", "air", "air", "air", "air", "t_altar"},
			{"t_altar", "air", "air", "air", "air", "air", "air", "air", "air", "air", "air", "air", "t_altar"},
			{"", "t_altar", "air", "air", "air", "air", "air", "air", "air", "air", "air", "t_altar", ""},
			{"", "t_altar", "air", "air", "air", "air", "air", "air", "air", "air", "air", "t_altar", ""},
			{"", "", "t_altar", "air", "air", "air", "air", "air", "air", "air", "t_altar", "", ""},
			{"", "", "", "t_altar", "t_altar", "air", "air", "air", "t_altar", "t_altar", "", "", ""},
			{"", "", "", "", "", "t_altar", "t_altar", "t_altar", "", "", "", "", ""},
		},
	},
	function(node, player)
		if not magic.is_near_to_magic(node.under, 10) then
			minetest.chat_send_player(player:get_player_name(), "[Mod Magic] Needs to be near to a magic source")
			return
		end
		
		magic.do_ritual(node, player)
	end
)