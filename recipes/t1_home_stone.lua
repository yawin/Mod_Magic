-----------------------------------------------------------------
-- HOME STONE RECIPE --------------------------------------------
-----------------------------------------------------------------

magic.register_recipe("home_stone",
	{
		{
			{"","","","","","","","","","","","",""},
			{"","","","","","","","","","","","",""},
			{"","","","","","","","","","","","",""},
			{"","","","","","","","","","","","",""},
			{"","","","","","","","","","","","",""},
			{"","","","","","","","","","","","",""},
			{"","","","","","","","","","","","",""},
			{"","","","","","","","","","","","",""},
			{"","","","","","","","","","","","",""},
			{"","","","","","","","","","","","",""},
			{"","","","","","","","","","","","",""},
			{"","","","","","","","","","","","",""},
			{"","","","","","","","","","","","",""},
		},
		{
			{"","","","","","","default:cobble","","","","","",""},
			{"","","","default:cobble","air","air","air","air","air","default:cobble","","",""},
			{"","","air","air","air","air","air","air","air","air","air","",""},
			{"","default:cobble","air","air","air","air","air","air","air","air","air","default:cobble",""},
			{"","air","air","air","air","air","air","air","air","air","air","air",""},
			{"air","air","air","air","air","air","stairs:slab_cobble","air","air","air","air","air","air"},
			{"default:cobble","air","air","air","air","stairs:slab_cobble","default:obsidian","stairs:slab_cobble","air","air","air","air","default:cobble"},
			{"air","air","air","air","air","air","stairs:slab_cobble","air","air","air","air","air","air"},
			{"","air","air","air","air","air","air","air","air","air","air","air",""},
			{"","default:cobble","air","air","air","air","air","air","air","air","air","default:cobble",""},
			{"","","air","air","air","air","air","air","air","air","air","",""},
			{"","","","default:cobble","air","air","air","air","air","default:cobble","","",""},
			{"","","","","","","default:cobble","","","","","",""},
		},
		{
			{"","","","","","","default:cobble","","","","","",""},
			{"","","","air","air","air","air","air","air","air","","",""},
			{"","","air","air","air","air","air","air","air","air","air","",""},
			{"","air","air","air","air","air","air","air","air","air","air","air",""},
			{"","air","air","air","air","air","air","air","air","air","air","air",""},
			{"air","air","air","air","air","air","air","air","air","air","air","air","air"},
			{"default:cobble","air","air","air","air","air","air","air","air","air","air","air","default:cobble"},
			{"air","air","air","air","air","air","air","air","air","air","air","air","air"},
			{"","air","air","air","air","air","air","air","air","air","air","air",""},
			{"","air","air","air","air","air","air","air","air","air","air","air",""},
			{"","","air","air","air","air","air","air","air","air","air","",""},
			{"","","","air","air","air","air","air","air","air","","",""},
			{"","","","","","","default:cobble","","","","","",""},
		},
	},
	
	function(node, player)
		if not magic.is_near_to_magic(node.under, 10) then
			minetest.chat_send_player(player:get_player_name(), "[Mod Magic] Needs to be near to a magic source")
			return
		end
		
		minetest.set_node(node.under, {name="magic:magicalobsidian"})
		minetest.env:add_item({x = node.under.x, y = node.under.y + 1, z = node.under.z}, "magic:home_stone")

		magic.gfx.recipe_ok(node.under)
	end
)