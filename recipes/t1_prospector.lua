-----------------------------------------------------------------
-- PROSPECTOR RECIPE --------------------------------------------
-----------------------------------------------------------------

magic.register_recipe("prospector",
	{
		{
			{"", "default:cobble", "","default:cobble",""},
			{"default:cobble", "", "default:gravel","","default:cobble"},
			{"", "default:gravel", "tier1","default:gravel",""},
			{"default:cobble", "", "default:gravel","","default:cobble"},
			{"", "default:cobble", "","default:cobble",""},
		},
	},
	function(node, player)
		local posx = node.under.x
		local posy = node.under.y
		local posz = node.under.z
		
		local tmpx = -2
		local tmpz = -2
		
		local initialy = posy
		local found = 0 --If 1: found, 2: too far
		while found == 0 do
			posy = posy - 1
			
			tmpx = -2
			while tmpx <= 2 and found == 0 do
				tmpz = -2
				while tmpz <= 2 and found == 0 do
					if minetest.get_node({x = posx + tmpx, y = posy, z = posz + tmpz}).name == "air" then
						found = 1
					end

					--minetest.set_node({x = posx + tmpx, y = posy, z = posz + tmpz}, {name = "air"})

					tmpz = tmpz + 1
				end
				tmpx = tmpx + 1
			end
			
			
			if initialy - 1000 > posy then
				found = 2
			end
		end
		
		if found == 1 then
			minetest.chat_send_player(player:get_player_name(), "[Mod Magic] Founded a cave " .. initialy - posy .. " blocks under you")
		else
			minetest.chat_send_player(player:get_player_name(), "[Mod Magic] There is not a cave or is too far")
		end

		magic.gfx.recipe_ok(node.under)
	end
) 
