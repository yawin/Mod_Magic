-----------------------------------------------------------------
-- BEGINING RECIPE (Creates the Magical Water Source) -----------
-----------------------------------------------------------------

magic.register_recipe("begining",
	{
		{
			{"", "", "", "default:cobble", "", "", "",},
			{"", "", "default:cobble", "default:cobble", "default:cobble", "", "",},
			{"", "default:cobble", "air", "default:cobble", "air", "default:cobble", "",},
			{"default:cobble", 	"default:cobble", "default:cobble", "default:leaves", "default:cobble", "default:cobble", "default:cobble",},
			{"", "default:cobble", "air", "default:cobble", "air", "default:cobble", "",},
			{"", "", "default:cobble", "default:cobble", "default:cobble", "", "",},
			{"", "", "", "default:cobble", "", "", "",},
		},
	},
	function(node)
		local posx = node.under.x - 3
		local posy = node.under.y
		local posz = node.under.z - 3

		local sch = magic.path .. "/schems/magical_font.mts"
		minetest.place_schematic({x = posx, y = posy, z = posz}, sch, 0, nil, true)

		magic.gfx.recipe_ok(node.under)
	end
)