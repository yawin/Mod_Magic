-----------------------------------------------------------------
-- PROSPECTOR RECIPE --------------------------------------------
-----------------------------------------------------------------

magic.register_recipe("perforator",
	{
		{
			{"", "",""},
			{"", "default:obsidian",""},
			{"", "", ""},
		},
	},
	function(node, player, facedir)
		local posx = node.under.x
		local posy = node.under.y
		local posz = node.under.z

		local n = ""
		local modif = {x = 2, y = 2, z = 2}
		if facedir.x ~= 0 then
			modif.x = 0
			n = minetest.get_node({x = posx, y = posy, z = posz + 1}).name
			if minetest.get_node({x = posx, y = posy, z = posz - 1}).name ~= n then
				return
			end
		elseif facedir.z ~= 0 then
			modif.z = 0
			n = minetest.get_node({x = posx + 1, y = posy, z = posz}).name
			if minetest.get_node({x = posx - 1, y = posy, z = posz}).name ~= n then
				return
			end
		elseif facedir.y ~= 0 then
			modif.y = 0
			n = minetest.get_node({x = posx - 1, y = posy, z = posz}).name
			if n ~= "air" then
				if minetest.get_node({x = posx + 1, y = posy, z = posz}).name ~= n then
					return
				end
			else
				n = minetest.get_node({x = posx, y = posy, z = posz - 1}).name
				if minetest.get_node({x = posx, y = posy, z = posz + 1}).name ~= n then
					return
				end
			end
		end

		local range = 5
		if n == "default:coalblock" then
			 range = range * 2
		elseif n == "default:goldblock" then
			 range = range * 4
		elseif n == "default:diamondblock" then
			 range = range * 8
		else
			return
		end

		local item = ""
		for i = 0, range do
			for eq = modif.x*-1, modif.x do
				for iy = modif.y*-1, modif.y do
					for zz = modif.z*-1, modif.z do
						item = minetest.get_node({x = posx + eq + facedir.x*i, y = posy + iy + facedir.y*i, z = posz + zz + facedir.z*i})
						if item.name ~= "air" and item.description ~= "" then
							if i ~= 0 then
								minetest.env:add_item({x=posx + (math.random(-3,3)/10),y=posy+(math.random(25,75)/100),z=posz + (math.random(-3,3)/10)}, minetest.get_node_drops(item.name)[1])
							end
							
							minetest.set_node({x = posx + eq + facedir.x*i, y = posy + iy+ facedir.y*i, z = posz + zz + facedir.z*i},{name = "air"})
						end
					end
				end
			end
		end

		magic.gfx.recipe_ok(node.under)
	end
)