magic.register_ritual("hopperbloom",
	{
		{name = "flowers:rose", val = 1},
		{name = "default:chest", val = 1},
		{name = "default:steel_ingot", val = 5}
	},
	
	function(node, player)
		magic.clear_altar(node)
		minetest.env:add_item(node.under, "magic:hopperbloom")
	end
)